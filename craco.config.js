const path = require("path");

module.exports = {
  webpack: {
    alias: {
      "@": path.resolve(__dirname, "src/"),
      "@ai-modules-homepage": path.resolve(__dirname, "src/modules/Homepage"),
      "@ai-components-navbar": path.resolve(__dirname, "src/components/Navbar"),
      "@ai-components-tombol": path.resolve(__dirname, "src/components/Tombol"),
    },
  },
  style: {
    postcss: {
      plugins: [require("tailwindcss"), require("autoprefixer")],
    },
  },
};
