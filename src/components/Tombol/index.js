import React from "react";

const Tombol = ({ text, onClick, color }) => {
  return (
    <div onClick={onClick} style={{ backgroundColor: color, width: '200px' }}>
      {text}
    </div>
  );
};

export default Tombol;
