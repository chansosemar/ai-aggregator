import React, { useState } from "react";

const Navbar = ({param1, param2}) => {
  const [active, setActive] = useState(0);
  const tabLabel = [{ text: param1 }, { text: param2 }];

  return (
    <div style={{ backgroundColor: "#47b2e4" }} className="p-10">
      <div className="flex justify-end">
        {tabLabel.map((e, i) => (
          <p
            onClick={() => setActive(i)}
            style={active === i ? { color: "red" } : { color: "white" }}
            className="font-bold mx-10 cursor-pointer"
          >
            {e.text}
          </p>
        ))}
      </div>
    </div>
  );
};

export default Navbar;
