import React from "react";
import Homepage from "@ai-modules-homepage";

function App() {
  return (
    <div
      className="d-flex justify-center align-items"
      style={{ backgroundColor: "#222" , height: '100vh'}}
    >
      <Homepage />
    </div>
  );
}

export default App;
