import React, { useState } from "react";
import Navbar from "@ai-components-navbar";
import Tombol from "@ai-components-tombol";

const Homepage = () => {
  const [data, setData] = useState('HOME');

  return (
    <div>
      <Navbar param1={data} param2="SERVICE" />
      <input
        placeholder="test"
        value={data}
        onChange={(e) => setData(e.target.value)}
      />
    </div>
  );
};

export default Homepage;
